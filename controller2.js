angular.module('myListApp', [])
    .controller('ListController', function ($scope, $http, $window) {
        var endpoint = "https://dev.tuten.cl:443/TutenREST/rest/user/contacto%40tuten.cl/bookings?current=true";
        $scope.bookings = [];
        console.log($scope.bookings);
        if (sessionStorage.getItem('session')) {

            var session = JSON.parse(sessionStorage.getItem('session'));
            console.log(session.data.sessionTokenBck);
            $http({
                method: 'GET',
                url: endpoint,
                headers: {
                    'adminemail': 'testapis@tuten.cl',
                    'token': session.data.sessionTokenBck,
                    'app': 'APP_BCK'
                }
            }).then(function (response) {
                for (var i = response.data.length - 1; i >= 0; i--) {
                    console.log(response.data[i]);
                    item = {};
                    item['bookingId'] = response.data[i].bookingId;
                    item['bookingTime'] = timeConverter(response.data[i].bookingTime);
                    item['bookingPrice'] = response.data[i].bookingPrice;
                    item['streetAddress'] = response.data[i].locationId.streetAddress;
                    item['firstName'] = response.data[i].tutenUserClient.firstName;
                    $scope.bookings.push(item);
                }
            }, function (error) {
                console.log(error);
            });
            var bookingsTemp = $scope.bookings;

            $scope.update = function () {
                console.log("criterio: " + $scope.criterio);
                console.log("tipo: " + $scope.tipo);
                $scope.bookings = bookingsTemp;
                if ($scope.tipo == ">=") {
                    if ($scope.columna == "BookingId") {
                        $scope.bookings = $scope.bookings.filter(book => book.bookingId >= $scope.criterio);
                    } else if ($scope.columna == "Precio") {
                        $scope.bookings = $scope.bookings.filter(book => book.bookingPrice >= $scope.criterio);
                    }
                } else if ($scope.tipo == "<=") {
                    if ($scope.columna == "BookingId") {
                        $scope.bookings = $scope.bookings.filter(book => book.bookingId <= $scope.criterio);
                    } else if ($scope.columna == "Precio") {
                        $scope.bookings = $scope.bookings.filter(book => book.bookingPrice <= $scope.criterio);
                    }

                } else if ($scope.tipo == "=") {
                    if ($scope.columna == "BookingId") {
                        $scope.bookings = $scope.bookings.filter(book => book.bookingId == $scope.criterio);
                    } else if ($scope.columna == "Precio") {
                        $scope.bookings = $scope.bookings.filter(book => book.bookingPrice == $scope.criterio);
                    }
                }
            }

        } else {
            $window.location.href = '/bookings/index.html';
        }

        function timeConverter(UNIX_timestamp) {
            var a = new Date(UNIX_timestamp * 1000);
            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var year = a.getFullYear();
            var month = months[a.getMonth()];
            var date = a.getDate();
            var hour = a.getHours();
            var min = a.getMinutes();
            var sec = a.getSeconds();
            var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
            return time;
        }
    });