angular.module('formLabs', [])
    .controller('UserController', function ($scope, $http, $window) {
        $scope.user = {};
        var endpoint = "https://dev.tuten.cl:443/TutenREST/rest/user/";

        $scope.update = function () {
            console.log($scope.user);
            var email = $scope.user.email;
            email = email.replace("@", "%40");
            endpoint = endpoint + email;
            $http({
                method: 'PUT',
                url: endpoint,
                headers: {
                    'password': $scope.user.password,
                    'app': $scope.user.app
                }
            }).then(function (response) {
                $scope.reset();
                console.log(response);
                sessionStorage.setItem("session", JSON.stringify(response));
                $window.location.href = '/bookings/tabla.html';
            }, function (error) {
                console.log(error);
                alert("Datos Incorrectos: " + error.data);
            });
        };

        $scope.reset = function (form) {
            $scope.user = {};
            if (form) {
                form.$setPristine();
                form.$setUntouched();
            }
        };

        $scope.reset();
    });